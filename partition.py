from dataclasses import dataclass

@dataclass(slots=True)
class Position:
    _container: any
    _data: any
    _size: int
    _parent: any

    def __init__(self, container, data) -> None:
        self._size = 1
        self._container =container
        self._data = data
        self._parent = self

    def get_data(self):
        return self._data()


class Partition:

    def make_group(self, data):
        return Position(self, data)
    
    def find(self, p):
        if p._parent != p:
            p._parent = self.find(p._parent)
        return p._parent
    
    def union(self, p, q):
        a = self.find(p)
        b = self.find(q)
        if a is not b:
            if a._size > b._size:
                b._parent = a
                a._size += b._size
            else:
                a._parent = b
                b._size += a._size
