from dataclasses import dataclass
from queue import Empty
import math
from io import StringIO

@dataclass(slots=True)
class QueueItem:
        key: any
        value:any
        def __lt__(self,other):
            return self.key < other.key
         
class PriorityQueueBase:
    def is_empty(self) -> bool:
        '''Return True if priority queue is empty'''
        return len(self) == 0

class HeapPriorityQueue(PriorityQueueBase):
    
    def __init__(self):
        self.data = []

    def __len__(self) -> int :
        return len(self.data)

    def _parent(self,i: int) -> int:
        '''Return index of parent node'''
        return (i-1)//2
    
    def _left(self, i: int) -> int:
        '''Return index of left sinblings node'''
        return 2*i+1
    
    def _right(self, i:int ) -> int:
        '''Return index of right sinblings node'''
        return 2*i+2

    def _has_left(self, i:int ) -> bool:
        '''Return true if node has left sibling node'''
        # index is beyond enf of list
        return self._left(i) < len(self.data)

    def _has_right(self, i:int ) -> bool:
        '''Return true if node has right sibling node'''
        # index is beyond enf of list
        return self._right(i) < len(self.data)
    
    def _swap(self, i:int , j:int ):
        '''Swap values on certain indexes'''
        self.data[i], self.data[j]  = self.data[j], self.data[i]
    
    def _upheap(self, i:int ):
        '''Upheap heap'''
        parent = self._parent(i)
        if i > 0 and self.data[i] < self.data[parent]:
            self._swap(i, parent)
            # recur at position of parent
            self._upheap(parent)

    def _downheap(self, i:int ):
        '''Downheap heap'''
        if self._has_left(i):
            left = self._left(i)
            child = left
            if self._has_right(i):
                right = self._right(i)
                if self.data[right] < self.data[left]:
                    child = right
            if self.data[child] < self.data[i]:
                self._swap(i, child)
                self._downheap(child)
    
    def add(self, key, value):
        """Add item to priority queue"""
        self.data.append(QueueItem(key,value))
        self._upheap(len(self.data)-1)
    
    def min(self):
        """Return but NOT REMOVE tuple (key, value) with minimum key"""
        if self.is_empty():
            raise Empty('Priority queue is empty.')
        result = self.data[0]
        return (result.key, result.value)

    def remove_min(self):
        """Return but AND REMOVE tuple (key, value) with minimum key"""
        if self.is_empty():
            raise Empty('Priority queue is empty')
        self._swap(0, len(self.data)-1)
        result = self.data.pop()
        self._downheap(0)
        return (result.key, result.value)

    def print(self, total_width=60, fill=' '):
        """Pretty-print a tree. total_width depends on your input size"""
        output = StringIO()
        last_row = -1
        for i, node in enumerate(self.data):
            if i:
                row = int(math.floor(math.log(i+1, 2)))
            else:
                row = 0
            if row != last_row:
                output.write('\n')
            columns = 2**row
            col_width = int(math.floor((total_width * 1.0) / columns))
            output.write(str('('+ str(node.key)+ ',' +  str(node.value) + ')' ).center(col_width, fill))
            last_row = row
        print (output.getvalue())
        print ('-' * total_width)

class IndexedItem(QueueItem):
    __slots__ = 'index'
    def __init__(self, key, value, index):
        super().__init__(key, value)
        self.index = index

class IndexedPriorityQueue(HeapPriorityQueue):
    def add(self, key: any, value: any) -> IndexedItem:
        '''Add item into indexed priority queue'''
        item = IndexedItem(key, value, len(self.data))
        self.data.append(item)
        self._upheap(len(self.data)-1)
        return item
    
    def _buble(self, i: int):
        '''Buble indexed heape'''
        if i > 0 and self.data[i] < self.data[self._parent(i)]:
            self._upheap(i)
        else:
            self._downheap(i)
    
    def update(self, indexedItem: IndexedItem, new_key: any, new_value: any):
        '''Update value on certain index'''
        i= indexedItem.index
        if not (0 <= i < len(self) and self.data[i] == indexedItem):
            raise ValueError('Invalid locator')

        indexedItem.key = new_key
        indexedItem.value = new_value
        self._buble(i)

    def _swap(self, i: int, j: int):
        '''Swap values on certain indexes and set right indexes'''
        super()._swap(i,j)
        self.data[i].index =  i
        self.data[j].index =  j