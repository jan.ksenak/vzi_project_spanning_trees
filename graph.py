from dataclasses import dataclass
from heap import IndexedPriorityQueue, HeapPriorityQueue
from partition import Partition
@dataclass(slots=True)
class Vertex:
    _data: any

    def get_data(self) ->any:
        '''Return data of vertex'''
        return self._data

    def __hash__(self):
        return hash(self._data)

    def __eq__(self, other):
        return self._data == other
@dataclass(slots=True)
class Edge:
    _origin: Vertex
    _destination: Vertex
    _weight: int

    def get_weight(self) -> any:
        '''Return weight/cost of edge/path'''
        return self._weight

    def __hash__(self) -> int:
        return hash((self._origin, self._destination))

    def get_endpoints(self) -> tuple[Vertex, Vertex]:
        '''Return edge endpoints as tuple(origin, destination)'''
        return (self._origin, self._destination)

    def get_opposite(self, vertex: Vertex) -> Vertex:
        '''Return destination of edge'''
        return self._destination if vertex == self._origin else self._origin
class Graph:
    _outgoing: dict
    _incoming: dict

    def __init__(self, directed: bool = False) -> None:
        self._outgoing = {}
        self._incoming = {} if directed else self._outgoing

    def is_directed(self) -> bool:
        '''Return true if graph is directed'''
        return self._incoming is not self._outgoing

    def vertex_count(self) -> int:
        '''Return count of vertexes'''
        return len(self._outgoing)

    def vertices(self):
        "Return iteration of all vertex in graph"
        return self._outgoing.keys()

    def edge_count(self) -> int:
        '''Return count of edges'''
        total = sum(len(self._outgoing[vertex]) for vertex in self._outgoing)
        return total if self.is_directed() else total // 2

    def get_edges(self) -> set:
        '''Return set of all edges'''
        result = set()
        for map in self._outgoing.values():
            result.update(map.values())
        return result

    def degree(self, vertex: Vertex, outgoing=True) -> int:
        '''For an undirected graph, return the number of edges incident to vertex. For a directed graph, return the number of outgoing (resp. incoming) edges incident to vertex v'''
        adj = self._outgoing if outgoing else self._incoming
        return len(adj[vertex])

    def incident_edges(self, vertex: Vertex, outgoing=True) -> list:
        '''Return an iteration of all edges incident to vertex v. In the case of a directed graph, report outgoing edges by default'''
        adj = self._outgoing if outgoing else self._incoming
        result = []
        for edge in adj[vertex].values():
            result.append(edge)
        return result

    def insert_vertex(self, x: any=None) -> Vertex:
        '''Insert vertex to graph'''
        vertex = Vertex(x)
        self._outgoing[vertex] = {}
        if self.is_directed():
            self._incoming[vertex] = {}
        return vertex

    def insert_edge(self, vertex_u:Vertex , vertex_v:Vertex , x:any=None):
        '''Insert edge between two vertexes in graph '''
        edge = Edge(vertex_u, vertex_v, x)
        self._outgoing[vertex_u][vertex_v] = edge
        self._incoming[vertex_v][vertex_u] = edge

    def MST_PrimJarnik(self):
        '''Implementation of Prim's\Jarnik's algorithm for minimum spanning tree of graphs'''
        # Init phase
        tree = []
        d = {}
        pq = IndexedPriorityQueue()
        pq_index = {}
        
        for vertex in self.vertices():
            if len(d) == 0:
                d[vertex] = 0
            else:
                d[vertex] = float('inf')
            pq_index[vertex] = pq.add(d[vertex], (vertex, None))

        while not pq.is_empty():
            key, value = pq.remove_min()
            vertex_u, edge = value
            pq_index.pop(vertex_u)
        
            if edge is not None:
                tree.append(edge)

            for link in self.incident_edges(vertex_u):
                vertex_v = link.get_opposite(vertex_u)
                if vertex_v in pq_index:
                    weight = link.get_weight()
                    if weight < d[vertex_v]:
                        d[vertex_v] = weight
                        pq.update(pq_index[vertex_v], d[vertex_v], (vertex_v,link))
        return tree  
        
    def MST_Kruskal(self):
        '''Implementation of Kruskal algorithm for minimum spanning tree of graphs'''
        # Init phase
        tree = []
        pq = HeapPriorityQueue()
        forest = Partition()
        position = {}

        for vertex in self.vertices():
            position[vertex] = forest.make_group(vertex)

        for edge in self.get_edges():
            pq.add(edge.get_weight(), edge)
        
        size = self.vertex_count()
        while len(tree) != (size - 1) and not pq.is_empty():
            weight, edge = pq.remove_min()
            vertex_u, vertex_v = edge.get_endpoints()
            a = forest.find(position[vertex_u])
            b = forest.find(position[vertex_v])
            if a != b:
                tree.append(edge)
                forest.union(a,b)
        return tree

    




