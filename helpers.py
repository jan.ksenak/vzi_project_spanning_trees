from constants import PRIM_ALGO_TEST_CODE, KRUSKAL_ALGO_TEST_CODE
import timeit


def algo_test():
    print('--------------------------------------------------------------------------------------------------------------------------------------------------')
    print('Time comparasion between Prim’s/Jarnik’s and Kruskal’s algorithm for minimum spanning trees of graphs')
    print('Graph is represented as Adjacency List')
    prim_time = round(timeit.timeit(PRIM_ALGO_TEST_CODE, number=10000), 3)
    kruskal_time = round(timeit.timeit(KRUSKAL_ALGO_TEST_CODE, number=10000), 3)
    print('Prim’s/Jarnik’s algorithm  - 10000 loops',prim_time, 's')
    print('Kruskal’s algorithm - 10000 loops', kruskal_time, 's')
    print('--------------------------------------------------------------------------------------------------------------------------------------------------')
    print('-------------------------------------------------------Winner-------------------------------------------------------------------------------------------')

    if prim_time < kruskal_time:
        winner_time = prim_time 
        winner_name = 'Prim’s/Jarnik’s algorithm' 
        performance_increase = round(((kruskal_time - prim_time) / prim_time * 100),3)
    else:
        winner_time = kruskal_time
        winner_name = 'Kruskal’s algorithm'
        performance_increase = round(((prim_time - kruskal_time) / kruskal_time * 100),3)

    
    print('Winners algorithm is', winner_name, 'with time', winner_time, 's')
    print('Percentage differance in performance is aprox.', performance_increase, '%')
    
    
