from graph import Graph
from helpers import algo_test

def main():
    test_graph = Graph()
    test_graph.insert_vertex(1)
    test_graph.insert_vertex(2)
    test_graph.insert_vertex(3)
    test_graph.insert_vertex(4)
    test_graph.insert_vertex(5)
    test_graph.insert_vertex(6)
    test_graph.insert_vertex(7)
    test_graph.insert_vertex(8)
    test_graph.insert_vertex(9)
    test_graph.insert_vertex(10)
    test_graph.insert_vertex(11)
    test_graph.insert_vertex(12)
    test_graph.insert_vertex(13)
    test_graph.insert_vertex(14)
    test_graph.insert_vertex(15)
    test_graph.insert_vertex(16)
    test_graph.insert_vertex(17)
    test_graph.insert_vertex(18)
    test_graph.insert_vertex(19)
    test_graph.insert_vertex(20)
    test_graph.insert_edge(1, 2, 12)
    test_graph.insert_edge(1, 3, 4)
    test_graph.insert_edge(1, 4, 8)
    test_graph.insert_edge(2, 10, 7)
    test_graph.insert_edge(3, 5, 14)
    test_graph.insert_edge(3, 20, 9)
    test_graph.insert_edge(3, 6, 7)
    test_graph.insert_edge(4, 2, 7)
    test_graph.insert_edge(5, 19, 2)
    test_graph.insert_edge(6, 11, 15)
    test_graph.insert_edge(6, 13, 17)
    test_graph.insert_edge(7, 6, 5)
    test_graph.insert_edge(7, 4, 12)
    test_graph.insert_edge(8, 17, 4)
    test_graph.insert_edge(9, 18, 11)
    test_graph.insert_edge(10, 11, 24)
    test_graph.insert_edge(11, 12, 24)
    test_graph.insert_edge(12, 8, 8)
    test_graph.insert_edge(12, 15, 1)
    test_graph.insert_edge(13, 14, 24)
    test_graph.insert_edge(14, 12, 1)
    test_graph.insert_edge(15, 16, 2)
    test_graph.insert_edge(16, 8, 3)
    test_graph.insert_edge(17, 7, 16)
    test_graph.insert_edge(17, 18, 10)
    test_graph.insert_edge(18, 5, 7)
    test_graph.insert_edge(19, 9, 3)
    test_graph.insert_edge(20, 19, 5)
    

    
    algo_test()
    
    print('\n--- Prim/Jarnik edge list ---')
    for edge in test_graph.MST_PrimJarnik():
          print(edge)
          
  
    print('\n--- Kruskal edge list ---')
    for edge in test_graph.MST_Kruskal():
        
        print(edge)
    


if __name__ == "__main__":
    main()

    
    

   
        
    
    
   