# Porovnanie Prim/Jarnikov algoritmu  a Kruskalov algoritmu pre výpočet mimálnej kostry grafu

**Graf** je reprezentovaný ako dátova štruktúra incidečná mapa (*Adjancy map*). Kde pre každy uzol grafu, využijeme  Zoznam vrcholo je nahradený na najvyššiej úrovni Python dátovou štrukúrov slovník (*Dictionary*), kde každému uzlu (kľúč) je namapovaná incidečná mapa (hodnota).

## Minimálna kostra grafu
- **Kostra grafu** - je podgraf, ktorý je stromom a obsahuje všetkz vrcholy grafu
- je daný súvisli graf G, kde jeho hrany sú ohodnotené realnými číslami, ktoré budeme nazývať cenami. **Kostrou grafu**, ktorá ma nejmenší šúčeť ohodnotený hran medzi všetkými kostrami, nazývame **minimálna kostra grafu**.

## Prim/Jarnikov algoritmus
Začíname s nejakým uzlom *S*, ktorý deﬁnuje počiatočný zhluk uzlov *C* . Potom v každej iterácii vyberieme hranu minimálnej váhy *e =(u,v)*, ktorá spája uzol *V*  v zhluke *C* s uzlom *U* mimo zhluk C. Uzol *V* sa potom prenesie do zhluku *C* a proces sa opakuje, kým sa nevytvorí preklenovací strom.

#### Implementácia Prim/Jarnikov algoritmu
Implementácia sa spolieha na *indexovateľnú prioritnú frontu založenej na binarnej halde*. V počiatočnej fáze vykonáme *n* vkladov do fronty, neskôr vykonáme *n* operácií extrackie minimalnej hodnoty z fronty a v rámci algoritmu môžeme aktualizovať celkovo *m* priorít.

#### Pseudo kód
![Prim pseudo code.png](img/Prim_pseudo_code.png)
## Kruskalov algoritmus
Kruskalov algoritmus udržiava les zhlukov, pričom opakovane spája dvojice zhlukov, až kým graf nevytvorí jediný zhluk. 
Na začiatku je každý uzol sám o sebe jeden zhluk. Algoritmus potom postupne zvažuje každú hranu zoradenú podľa rastúcej ceny (Prioritná fronta). Ak hrana *E* spája dva rôzne zhluky, potom sa hrana *E* pridá do množiny hrán minimálneho stromu grafu  a dva zhluky spojené hranou *E* sa spoja do jedného zhluku. Ak naopak hrana *E* spája dva uzly, ktoré už sú v tom istom zhluku, potom sa hrana *E* vyradí. Keď algoritmus pridá dostatočný počet hrán na vytvorenie minimálnej kostry grafu, tak algoritmus končí

#### Implementácia Kruskalov algoritmu
Existujú 2 hlavné prkvy, ktoré zvyšujú časovú naročnosť tohto algoritmu. 
1. Uvožovať hrany v neklesajúcom poradí ich cien
2. Riadenie rozdelenia zhlukov
Prvý problem sme vyriešili implementovaním prioritnej fronty na základe binárnej haldy. Aby sme efektívne riešile riadenie rozdelenia zhlukov. musíme byť schopní nájsť zhluky pre uzly *U* a *V*, ktoré sú koncovými bodmi hrany *E*, otestovať, či sú tieto dva zhluky odlišné, a ak áno, zlúčiť tieto dva zhluky do jedného. Na tento problém sme implementovali dátovu štruktúru **Union-find**
#### Pseudo kód
![Kruskal pseudo code](img/Kruskal_pseudo_code.png)

# Výsledok porovnania

Meranie potrebné času na výpočet minimálnej kostry grafu prebiehalo následovne.
1. Obidva algoritmy dostali totožný graf o 20 uzlov.
2. Výpočet prebiehol 10 000x.
3. Vypísal sa časová naročnosť v sekundách.
#### Vstupný graf
![Graph](img/graph_img.png)
#### Výsledok porovnania
![Result](img/Result.png)
